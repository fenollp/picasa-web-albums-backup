#!/bin/bash

usage() {
    echo "Usage: $0  ‹Folder :: Destination› ‹File :: Picasa Web Album's “Special” RSS›"
    echo ""
    echo "  Why “Special”?"
    echo "In your browser, find the blue link to the album's RSS feed (bottom right-hand"
    echo "side). Copy link's address and add this key=value to it: `imgdl=1`; that is,"
    echo "append `&imgdl=1` to RSS's URL. Hit ‹Enter› to download the XML file."

    echo "Requires: wget."
    exit 1
}

[[ $# -ne 2 ]] && usage


CUR=$(pwd)
DIR="${1}"
RSS="${2}"
mkdir -p $DIR

album() {
    cat $RSS | grep -Po "http[^<>'\"]+" | uniq
}

album | grep /d/ | uniq > $DIR/imgs # Really needs a redundant uniq
album | grep googlevideo | sed 's/\&amp;/\&/g' | sed 's/%3D/:/g' > $DIR/videos

cd $DIR
cat imgs videos > list
wget -i list
rm list
cd $CUR

