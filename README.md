#Picasa Web Albums backup
A UNIX Shell script that accepts command-line requests to download
`picasa.com`'s pictures.

##Synopsis

    :::bash
    ./picasa.sh  ‹Folder :: Destination› ‹File :: Picasa Web Album's “Special” RSS›

###Case Study

Here you have an album you would like to backup, pick its address.
It should look like `https://picasaweb.google.com/102320533950727509059`.  
Now find the blue link to the album's RSS feed (bottom right-hand side).
Copy link's address and add this key=value to it: `imgdl=1`; that is,
append `&imgdl=1` to RSS's URL. Hit ‹Enter› to download the XML file.

Download the show with:

    :::bash
    ./arte+7.sh ~/Downloads 102320533950727509059.xml

##Requirements
* wget
